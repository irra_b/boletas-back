<?php

namespace TestBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Firebase\JWT\JWT;
use Exception;
use ModelBundle\Entity\Rubro;
use ModelBundle\Entity\Departamento;
use ModelBundle\Entity\Tipo;
use ModelBundle\Entity\Empresa;
use ModelBundle\Entity\Contacto;

class DefaultController extends FOSRestController {

    

    

    /**
     * @Route("/usuario/{id}", requirements={"id" = "\d+"}, defaults={"id" = 1})
     * @Method({"GET"})
     */
    public function getByIdAction($id) {
        $restresult = $this->getDoctrine()->getRepository('ModelBundle:Rubro')->findOneBy(array('id' => $id));
        if ($restresult === null) {
            return new View("there are no users exist", Response::HTTP_NOT_FOUND);
        }
        return $restresult;
    }

    

    
    

    

    
    

}
