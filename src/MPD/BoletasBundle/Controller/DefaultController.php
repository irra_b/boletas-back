<?php

namespace MPD\BoletasBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use ModelBundle\Entity\Boleta;
use ModelBundle\Entity\Banco;
use ModelBundle\Entity\Tipo;
use ModelBundle\Entity\Beneficiario;
use ModelBundle\Entity\Cumplimiento;
use ModelBundle\Entity\Ejecutado;
use ModelBundle\Entity\ArchivoCumplimiento;
use ModelBundle\Entity\ArchivoEjecutado;
use Firebase\JWT\JWT;
use Exception;

class DefaultController extends FOSRestController {

    /**
     * @Route("/")
     * @Method({"GET"})
     */
    public function indexAction(Request $request) {
        $req = $request->headers->get('authorization');
        $jwt = $this->get('utilidades');
        $data = $jwt->decode($req);
        if ($data['status'] != 'ok') {

            return new View($data, Response::HTTP_NOT_FOUND);
        }
        try {
            $this->updateVencidas();
            $fecha = new \DateTime();
            $em = $this->getDoctrine()->getManager();
            $restresult = $em->getConnection()->prepare('SELECT *,CONCAT(b.moneda,b.monto)as monto FROM boleta b WHERE b.eliminado=0 AND b.estado!=25 Order By b.fecha_valido asc');
            $restresult->execute();
            $r = array();
            if ($restresult === null) {
                return new View("Sin resultados", Response::HTTP_NOT_FOUND);
            }
            foreach ($restresult->fetchAll() as $value) {
                $childrens = $this->getParent($value['renovado_por']);
                $arch = $em->getRepository('ModelBundle:Adjuntos')->findBy(array('boleta' => $value['id']));
                $archivos = array();
                foreach ($arch as $a) {
                    array_push($archivos, array('id' => $a->getId(), 'name' => $a->getName(), 'size' => $a->getSize(), 'uri' => $a->getUri(), 'type' => $a->getType(), 'icon' => $a->getIcon()));
                }
                $d = $fecha->diff(new \DateTime($value['fecha_valido']));
                $value['childrens'] = $childrens;
                $value['adjuntos'] = $archivos;
                $value['cumplimiento'] = $this->getCumplimiento($value['id']);
                $value['ejecutado'] = $this->getEjecutado($value['id']);
                $value['days'] = $d->days + 1;
                $value['d'] = $d->d;
                $value['m'] = $d->m;
                $value['y'] = $d->y;
                $value['invert'] = $d->invert;
                array_push($r, $value);
            }
            return $r;
        } catch (Exception $e) {
            return new View($e->getMessage(), Response::HTTP_NOT_FOUND);
        }
        return new View("there are no users exist", Response::HTTP_NOT_FOUND);
    }

    private function updateVencidas() {

        $em = $this->getDoctrine()->getManager();
        $restresult = $em->getConnection()->prepare('SELECT * 
FROM (SELECT *,DATEDIFF(b.fecha_valido,curdate()) as dias
			FROM boleta b
			WHERE b.eliminado=0 AND b.estado IN(1,4,26))as t
WHERE t.dias<0');
        $restresult->execute();
        foreach ($restresult->fetchAll() as $value) {
            $estado = $this->getDoctrine()->getRepository('ModelBundle:Estado')->findOneBy(array('id' => 4));
            $boleta = $this->getDoctrine()->getRepository('ModelBundle:Boleta')->findOneBy(array('id' => $value['id']));
            $boleta->setEstado($estado);
            $em->persist($boleta);
            $em->flush();
        }
    }

    /**
     * @Route("/profile/")
     * @Method({"GET"})
     */
    public function getUserAction(Request $request) {
        $req = $request->headers->get('authorization');
        $jwt = $this->get('utilidades');
        $data = $jwt->decode($req);
        if ($data['status'] != 'ok') {

            return new View($data, Response::HTTP_NOT_FOUND);
        }
        try {
            $em = $this->getDoctrine()->getManager();
            $restresult = $em->getConnection()->prepare('SELECT u.email,u.nombre,u.cargo,u.rol,u.img
FROM user u
WHERE u.id=:id');
            $restresult->execute(array('id' => $data['user']));
            return new View(array('status' => Response::HTTP_OK, 'data' => $restresult->fetchAll()), Response::HTTP_OK);
        } catch (Exception $e) {
            return new View(array('status' => Response::HTTP_NOT_FOUND, 'msg' => $e->getMessage()), Response::HTTP_OK);
        }
    }

    /**
     * @Route("/profile/")
     * @Method({"PUT"})
     */
    public function updateImgAction(Request $request) {
        $req = $request->headers->get('authorization');
        $jwt = $this->get('utilidades');
        $data = $jwt->decode($req);
        if ($data['status'] != 'ok') {

            return new View($data, Response::HTTP_NOT_FOUND);
        }
        try {
            $img = json_decode($request->getContent(), TRUE);
            $em = $this->getDoctrine()->getManager();
            $u = $this->getDoctrine()->getRepository('ModelBundle:User')->findOneBy(array('id' => $data['user']));
            $u->setImg($img['img']);
            $em->persist($u);
            $em->flush();
            return new View(array('status' => Response::HTTP_OK), Response::HTTP_OK);
        } catch (Exception $e) {
            return new View(array('status' => Response::HTTP_NOT_FOUND, 'msg' => $e->getMessage()), Response::HTTP_OK);
        }
    }

    /**
     * @Route("/dashboard/")
     * @Method({"GET"})
     */
    public function dashboardAction(Request $request) {
        $req = $request->headers->get('authorization');
        $jwt = $this->get('utilidades');
        $data = $jwt->decode($req);
        if ($data['status'] != 'ok') {

            return new View($data, Response::HTTP_NOT_FOUND);
        }
        try {
            $this->updateVencidas();
            $fecha = new \DateTime();
            $em = $this->getDoctrine()->getManager();
            $r = array();
            $restresult = $em->getConnection()->prepare('SELECT e.nombre as estado,COUNT(*) as total
FROM boleta b
INNER JOIN estado e ON e.id=b.estado
WHERE b.eliminado=0
GROUP BY b.estado');
            $restresult->execute();
            array_push($r, $restresult->fetchAll());
            $restresult = $em->getConnection()->prepare('SELECT "negro"as cantidad,COUNT(*)as total 
FROM (SELECT *,DATEDIFF(b.fecha_valido,curdate()) as dias
			FROM boleta b
			WHERE b.eliminado=0 AND b.estado IN(1,4,26))as t
WHERE t.dias<0
UNION
SELECT "rojo"as cantidad,COUNT(*)as total 
FROM (SELECT *,DATEDIFF(b.fecha_valido,curdate()) as dias
			FROM boleta b
			WHERE b.eliminado=0 AND b.estado IN(1,4,26))as t
WHERE t.dias<=15 AND t.dias>0
UNION
SELECT "naranja"as cantidad,COUNT(*)as total 
FROM (SELECT *,DATEDIFF(b.fecha_valido,curdate()) as dias
			FROM boleta b
			WHERE b.eliminado=0 AND b.estado IN(1,4,26))as t
WHERE t.dias<=20 AND t.dias>15
UNION
SELECT "verde"as cantidad,COUNT(*)as total 
FROM (SELECT *,DATEDIFF(b.fecha_valido,curdate()) as dias
			FROM boleta b
			WHERE b.eliminado=0 AND b.estado IN(1,4,26))as t
WHERE t.dias>20');
            $restresult->execute();
            array_push($r, $restresult->fetchAll());
            array_push($r, $this->getEnRojo());
            if ($r === null) {
                return new View("Sin resultados", Response::HTTP_NOT_FOUND);
            }

            return $r;
        } catch (Exception $e) {
            return new View($e->getMessage(), Response::HTTP_NOT_FOUND);
        }
        return new View("there are no users exist", Response::HTTP_NOT_FOUND);
    }

    private function getCumplimiento($boleta) {
        $em = $this->getDoctrine()->getManager();
        $restresult = $em->getConnection()->prepare('SELECT * FROM cumplimiento c WHERE c.boleta=:boleta');
        $restresult->execute(array('boleta' => $boleta));
        $r = array();
        foreach ($restresult->fetchAll() as $value) {
            $re = $em->getConnection()->prepare('SELECT * FROM archivo_cumplimiento c WHERE c.cumplimiento=:c');
            $re->execute(array('c' => $value['id']));
            $value['archivos'] = $re->fetchAll();
            array_push($r, $value);
        }
        return $r;
    }

    private function getEjecutado($boleta) {
        $em = $this->getDoctrine()->getManager();
        $restresult = $em->getConnection()->prepare('SELECT * FROM ejecutado e WHERE e.boleta=:boleta');
        $restresult->execute(array('boleta' => $boleta));
        $r = array();
        foreach ($restresult->fetchAll() as $value) {
            $re = $em->getConnection()->prepare('SELECT * FROM archivo_ejecutado c WHERE c.ejecutado=:c');
            $re->execute(array('c' => $value['id']));
            $value['archivos'] = $re->fetchAll();
            array_push($r, $value);
        }
        return $r;
    }

    private function getParent($value) {
        $padre = $this->getDoctrine()->getRepository('ModelBundle:Boleta')->findOneBy(array('id' => $value, 'eliminado' => 0));

        if ($padre) {
            $r = $this->getParent($padre->getRenovadoPor());
            $padre->setRenovadoPor(null);
            array_push($r, $padre);
            return $r;
        } else {
            return array();
        }
    }

    /**
     * @Route("/{id}")
     * @Method({"GET"})
     */
    public function getByIdAction(Request $request, $id) {
        $req = $request->headers->get('authorization');
        $jwt = $this->get('utilidades');
        $data = $jwt->decode($req);
        if ($data['status'] != 'ok') {

            return new View($data, Response::HTTP_NOT_FOUND);
        }
        try {
            $em = $this->getDoctrine()->getManager();
            $restresult = $this->getDoctrine()->getRepository('ModelBundle:Boleta')->findOneBy(array('id' => $id));
            $arch = $em->getRepository('ModelBundle:Adjuntos')->findBy(array('boleta' => $id));
            $archivos = array();
            foreach ($arch as $a) {
                array_push($archivos, array('id' => $a->getId(), 'name' => $a->getName(), 'size' => $a->getSize(), 'uri' => $a->getUri(), 'type' => $a->getType(), 'icon' => $a->getIcon()));
            }
            $data=array('boleta'=>$restresult,'archivos'=>$archivos);
            
            if ($data === null) {
                return new View("there are no users exist", Response::HTTP_NOT_FOUND);
            }

            return $data;
        } catch (Exception $e) {
            return new View($e->getMessage(), Response::HTTP_NOT_FOUND);
        }
        return new View("there are no users exist", Response::HTTP_NOT_FOUND);
    }

    /**
     * @Route("/{id}")
     * @Method({"DELETE"})
     */
    public function deleteAction(Request $request, $id) {
        $req = $request->headers->get('authorization');
        $jwt = $this->get('utilidades');
        $data = $jwt->decode($req);
        if ($data['status'] != 'ok') {

            return new View($data, Response::HTTP_NOT_FOUND);
        }
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction();
        try {
            $boleta = new Boleta();
            $boleta2 = new Boleta();
            $boleta = $this->getDoctrine()->getRepository('ModelBundle:Boleta')->findOneBy(array('id' => $id));
            if (!is_null($boleta->getRenovadoPor())) {
                $boleta2 = $this->getDoctrine()->getRepository('ModelBundle:Boleta')->findOneBy(array('id' => $boleta->getRenovadoPor()));
                $estado = $this->getDoctrine()->getRepository('ModelBundle:Estado')->findOneBy(array('id' => 1));
                $boleta2->setEstado($estado);
            }

            $boleta->setEliminado(1);

            $em->persist($boleta);
            $em->flush();
            $em->getConnection()->commit();
            return new View(array('status' => Response::HTTP_OK), Response::HTTP_OK);
        } catch (Exception $e) {
            $em->getConnection()->rollBack();
            return new View($e->getMessage(), Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * @Route("/")
     * @Method({"POST"})
     */
    public function postAction(Request $request) {
        $req = $request->headers->get('authorization');
        $jwt = $this->get('utilidades');
        $data = $jwt->decode($req);
        if ($data['status'] != 'ok') {

            return new View($data, Response::HTTP_NOT_FOUND);
        }
        $data = json_decode($request->getContent(), TRUE);
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction();
        try {
            $boleta = new Boleta();
            if ($data['renov'] == true) {
                $estado = $this->getDoctrine()->getRepository('ModelBundle:Estado')->findOneBy(array('id' => 25));
                $padre = $this->getDoctrine()->getRepository('ModelBundle:Boleta')->findOneBy(array('id' => $data['id']));
                $boleta->setRenovadoPor($padre);
                $padre->setEstado($estado);
                $estado = $this->getDoctrine()->getRepository('ModelBundle:Estado')->findOneBy(array('id' => 26));
            } else {
                $estado = $this->getDoctrine()->getRepository('ModelBundle:Estado')->findOneBy(array('id' => 1));
            }
            $banco = $this->getDoctrine()->getRepository('ModelBundle:Banco')->findOneBy(array('id' => $data['banco']['id']));
            $tipo = $this->getDoctrine()->getRepository('ModelBundle:Tipo')->findOneBy(array('id' => $data['tipo']['id']));
            $beneficiario = $this->getDoctrine()->getRepository('ModelBundle:Beneficiario')->findOneBy(array('id' => $data['beneficiario']['id']));
            $boleta->setBanco($banco);
            $boleta->setBeneficiario($beneficiario);
            $boleta->setCodigo($data['codigo']);
            $boleta->setConcepto($data['concepto']);
            $boleta->setSerie($data['serie']);
            foreach ($data['archivo'] as $archivo) {
                $a = new \ModelBundle\Entity\Adjuntos();
                $a->setName($archivo['name']);
                $a->setSize($archivo['size']);
                $a->setUri($archivo['uri']);
                $a->setType($archivo['type']);
                $a->setIcon('icon');
                $a->setBoleta($boleta);
                $em->persist($a);
            }
            $boleta->setEstado($estado);
            $boleta->setMoneda($data['moneda']['nombre']);
            $fechaValido = new \DateTime($data['fecha_valido']);
            $boleta->setFechaValido($fechaValido);
            $boleta->setMonto($data['monto']);
            $boleta->setProveedor($data['proveedor']);
            $boleta->setTipo($tipo);
            $boleta->setEliminado(0);
            $boleta->setObservaciones($data['observaciones']);
            $boleta->setEditable(0);
            $em->persist($boleta);
            $em->flush();
            $em->getConnection()->commit();
            return new View(array('status' => Response::HTTP_OK), Response::HTTP_OK);
        } catch (Exception $e) {
            $em->getConnection()->rollBack();
            return new View(array('status' => Response::HTTP_NOT_FOUND, 'msg' => $e->getMessage()), Response::HTTP_OK);
        }
    }

    /**
     * @Route("/")
     * @Method({"PUT"})
     */
    public function putAction(Request $request) {
        $req = $request->headers->get('authorization');
        $jwt = $this->get('utilidades');
        $data = $jwt->decode($req);
        if ($data['status'] != 'ok') {

            return new View($data, Response::HTTP_NOT_FOUND);
        }
        $data = json_decode($request->getContent(), TRUE);
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction();
        try {
            $boleta = $this->getDoctrine()->getRepository('ModelBundle:Boleta')->findOneBy(array('id' => $data['id']));
            $banco = $this->getDoctrine()->getRepository('ModelBundle:Banco')->findOneBy(array('id' => $data['banco']['id']));
            $tipo = $this->getDoctrine()->getRepository('ModelBundle:Tipo')->findOneBy(array('id' => $data['tipo']['id']));
            $beneficiario = $this->getDoctrine()->getRepository('ModelBundle:Beneficiario')->findOneBy(array('id' => $data['beneficiario']['id']));
            //$estado = $this->getDoctrine()->getRepository('ModelBundle:Estado')->findOneBy(array('id' => $data['estado']['id']));
            $archivos = $em->getRepository('ModelBundle:Adjuntos')->findBy(array('boleta' => $boleta->getId()));
            foreach ($archivos as $archivo) {
                $em->remove($archivo);
            }
            $boleta->setBanco($banco);
            $boleta->setBeneficiario($beneficiario);
            $boleta->setCodigo($data['codigo']);
            $boleta->setConcepto($data['concepto']);
            $boleta->setSerie($data['serie']);
            $boleta->setUri($data['archivo']['uri']);
            $boleta->setMoneda($data['moneda']['nombre']);
            $fechaValido = new \DateTime($data['fecha_valido']);
            $boleta->setFechaValido($fechaValido);
            $boleta->setMonto($data['monto']);
            $boleta->setProveedor($data['proveedor']);
            $boleta->setTipo($tipo);
            $boleta->setObservaciones($data['observaciones']);
            $boleta->setEditable(0);
            foreach ($data['archivo'] as $archivo) {
                $a = new \ModelBundle\Entity\Adjuntos();
                $a->setName($archivo['name']);
                $a->setSize($archivo['size']);
                $a->setUri($archivo['uri']);
                $a->setType($archivo['type']);
                $a->setIcon('icon');
                $a->setBoleta($boleta);
                $em->persist($a);
            }
            $em->persist($boleta);
            $em->flush();
            $em->getConnection()->commit();
            return new View(array('status' => Response::HTTP_OK), Response::HTTP_OK);
        } catch (Exception $e) {
            $em->getConnection()->rollBack();
            return new View(array('status' => Response::HTTP_NOT_FOUND, 'msg' => $e->getMessage()), Response::HTTP_OK);
        }
    }

    /**
     * @Route("/editable/")
     * @Method({"PUT"})
     */
    public function putEditableAction(Request $request) {
        $req = $request->headers->get('authorization');
        $jwt = $this->get('utilidades');
        $data = $jwt->decode($req);
        $user = $data;
        if ($data['status'] != 'ok') {

            return new View($data, Response::HTTP_NOT_FOUND);
        }
        $data = json_decode($request->getContent(), TRUE);
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction();
        try {
            if ($user['rol'] != 'ROLE_ADMIN') {
                throw new Exception('Usuario sin privilegios');
            }
            $boleta = $this->getDoctrine()->getRepository('ModelBundle:Boleta')->findOneBy(array('id' => $data));
            $boleta->setEditable(1);
            $em->persist($boleta);
            $em->flush();
            $em->getConnection()->commit();
            return new View(array('status' => Response::HTTP_OK), Response::HTTP_OK);
        } catch (Exception $e) {
            $em->getConnection()->rollBack();
            return new View(array('status' => Response::HTTP_NOT_FOUND, 'msg' => $e->getMessage()), Response::HTTP_OK);
        }
    }

    /**
     * @Route("/params/")
     * @Method({"GET"})
     */
    public function paramsAction(Request $request) {
        $req = $request->headers->get('authorization');
        $jwt = $this->get('utilidades');
        $data = $jwt->decode($req);

        if ($data['status'] != 'ok') {

            return new View($data, Response::HTTP_NOT_FOUND);
        }
        try {
            $bancos = $this->getDoctrine()->getRepository('ModelBundle:Banco')->findAll();
            $tipos = $this->getDoctrine()->getRepository('ModelBundle:Tipo')->findAll();
            $beneficiarios = $this->getDoctrine()->getRepository('ModelBundle:Beneficiario')->findAll();
            $estados = $this->getDoctrine()->getRepository('ModelBundle:Estado')->findAll();
        } catch (Exception $e) {
            return new View($e->getMessage() . '<<<', Response::HTTP_NOT_FOUND);
        }
        return array('bancos' => $bancos, 'tipos' => $tipos, 'beneficiarios' => $beneficiarios, 'estados' => $estados);
    }

    /**
     * @Route("/service/{dias}")
     * @Method({"GET"})
     */
    public function getOpenAction(Request $request, $dias) {
        //$req = $request->headers->get('authorization');
        //$jwt = $this->get('utilidades');
        //$data = $jwt->decode($req);
        //if ($data['status'] != 'ok') {
        //    return new View($data, Response::HTTP_NOT_FOUND);
        //}
        try {
            $fecha = new \DateTime();
            $restresult = $this->getData($dias);
            $r = array();
            if ($restresult === null) {
                return new View("there are no users exist", Response::HTTP_NOT_FOUND);
            }
            foreach ($restresult as $value) {
                $d = $fecha->diff(new \DateTime($value['fecha_valido']));
                $value['days'] = $d->days;
                $value['d'] = $d->d;
                $value['m'] = $d->m;
                $value['y'] = $d->y;
                array_push($r, $value);
            }
            return $r;
        } catch (Exception $e) {
            return new View($e->getMessage(), Response::HTTP_NOT_FOUND);
        }
        return new View("there are no users exist", Response::HTTP_NOT_FOUND);
    }

    private function getData($dias) {
        $em = $this->getDoctrine()->getManager();
        $restresult = $em->getConnection()->prepare('SELECT * 
FROM (
SELECT *,DATEDIFF(b.fecha_valido,curdate()) as dias
FROM boleta b
WHERE b.eliminado=0 AND b.estado IN(1,4,26))as t
WHERE t.dias<=:dias AND t.dias>0');
        $restresult->execute(array('dias' => $dias));
        return $restresult->fetchAll();
    }

    /**
     * @Route("/upload/")
     * @Method({"POST"})
     */
    public function uploadAction(Request $request) {
        ini_set('upload_max_filesize', '50M');
        ini_set('post_max_size', '50M');
        ini_set('max_input_time', 300);
        ini_set('max_execution_time', 300);
        ini_set('memory_limit', '512M');
        $req = $request->headers->get('authorization');
        $jwt = $this->get('utilidades');
        $data = $jwt->decode($req);
        if ($data['status'] != 'ok') {
            return new View($data, Response::HTTP_NOT_FOUND);
        }
        if (empty($_FILES) || $_FILES['files']['error'][0] != UPLOAD_ERR_OK) {
            //http_response_code(400);
            //die('{"OK": 0, "info": "Failed to move uploaded file."}');
            return new View('El archivo supera el límite permitido.', Response::HTTP_BAD_REQUEST);
        }
        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
        $fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : $_FILES["files"]["name"][0];
        $filePath = "uploads/" . $fileName;

        $out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
        if ($out) {
            // Read binary input stream and append it to temp file
            $in = @fopen($_FILES['files']['tmp_name'][0], "rb");

            if ($in) {
                while ($buff = fread($in, 4096))
                    fwrite($out, $buff);
            } else {
                http_response_code(400);
                die('{"OK": 0, "info": "Failed to open input stream."}');
            }

            @fclose($in);
            @fclose($out);

            @unlink($_FILES['files']['tmp_name'][0]);
        } else {
            http_response_code(400);
            die('{"OK": 0, "info": "Failed to open output stream."}');
        }

// Check if file has been uploaded
        if (!$chunks || $chunk == $chunks - 1) {
            // Strip the temp .part suffix off
            rename("{$filePath}.part", $filePath);
        }
        $info = pathinfo($filePath);
        die('{"OK": 1, "info": "Upload successful.","uri":"' . $filePath . '","size": "' . filesize($filePath) . '","type": "' . $info['extension'] . '","name": "' . $info['basename'] . '"}');
    }

    /**
     * @Route("/entidad/")
     * @Method({"POST"})
     */
    public function postEntidadAction(Request $request) {
        $req = $request->headers->get('authorization');
        $jwt = $this->get('utilidades');
        $data = $jwt->decode($req);
        if ($data['status'] != 'ok') {

            return new View($data, Response::HTTP_NOT_FOUND);
        }
        $data = json_decode($request->getContent(), TRUE);
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction();
        try {
            $banco = new Banco();
            $banco->setNombre($data['nombre']);
            $em->persist($banco);
            $em->flush();
            $em->getConnection()->commit();
            return new View(array('status' => Response::HTTP_OK), Response::HTTP_OK);
        } catch (Exception $e) {
            $em->getConnection()->rollBack();
            return new View(array('status' => Response::HTTP_NOT_FOUND, 'msg' => $e->getMessage()), Response::HTTP_OK);
        }
    }

    /**
     * @Route("/isfree/{banco}")
     * @Method({"GET"})
     */
    public function isFreeAction(Request $request, $banco) {
        $req = $request->headers->get('authorization');
        $jwt = $this->get('utilidades');
        $data = $jwt->decode($req);
        if ($data['status'] != 'ok') {

            return new View($data, Response::HTTP_NOT_FOUND);
        }
        try {
            $em = $this->getDoctrine()->getManager();
            $restresult = $em->getConnection()->prepare('SELECT count(*)as total FROM boleta b WHERE b.eliminado=0 AND b.banco=:banco');
            $restresult->execute(array('banco' => $banco));
            if ($restresult === null) {
                return new View("Sin resultados", Response::HTTP_NOT_FOUND);
            }
            return $restresult->fetchAll();
        } catch (Exception $e) {
            return new View($e->getMessage(), Response::HTTP_NOT_FOUND);
        }
        return new View("there are no users exist", Response::HTTP_NOT_FOUND);
    }

    /**
     * @Route("/entidad/{banco}")
     * @Method({"DELETE"})
     */
    public function delEntidadAction(Request $request, $banco) {
        $req = $request->headers->get('authorization');
        $jwt = $this->get('utilidades');
        $data = $jwt->decode($req);
        if ($data['status'] != 'ok') {

            return new View($data, Response::HTTP_NOT_FOUND);
        }
        $data = json_decode($request->getContent(), TRUE);
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction();
        try {
            $bancos = $this->getDoctrine()->getRepository('ModelBundle:Banco')->findOneBy(array('id' => $banco));
            $em->remove($bancos);
            $em->flush();
            $em->getConnection()->commit();
            return new View(array('status' => Response::HTTP_OK), Response::HTTP_OK);
        } catch (Exception $e) {
            $em->getConnection()->rollBack();
            return new View(array('status' => Response::HTTP_NOT_FOUND, 'msg' => $e->getMessage()), Response::HTTP_OK);
        }
    }

    /**
     * @return \TFox\MpdfPortBundle\Service\PDFService
     */
    private function getMpdfService() {
        return $this->get('t_fox_mpdf_port.pdf');
    }

    /**
     * @Route("/report/")
     * @Method({"POST"})
     */
    public function reportAction(Request $request) {
        $req = $request->headers->get('authorization');
        $jwt = $this->get('utilidades');
        $data = $jwt->decode($req);
        if ($data['status'] != 'ok') {

            return new View($data, Response::HTTP_NOT_FOUND);
        }
        $this->updateVencidas();
        $data = json_decode($request->getContent(), TRUE);
        $moneda = $data['moneda'];
        $data = $data['pick'];
        $d = '';
        foreach ($data as $value) {
            if ($value['id'] == 1) {
                $d .= '26,';
            }
            if ($value['id'] == 25) {
                $value['id'] = 26;
            }
            $d .= $value['id'] . ',';
        }
        $d = substr($d, 0, -1);

        $data = json_decode($request->getContent(), TRUE);
        try {
            $em = $this->getDoctrine()->getManager();
            $restresult = $em->getConnection()->prepare('SELECT b.id,
                b.proveedor,
                b.monto,
                b.moneda,
                b.codigo,
                b.serie,
                b.fecha_valido,
                b.concepto,
                t.nombre as tipo,
                ba.nombre as banco,
                IF(DATEDIFF(b.fecha_valido,curdate())<=0,"Vencido",e.nombre)as estado,
                b.observaciones,
                b.renovado_por,
                DATEDIFF(b.fecha_valido,curdate()) as dias
FROM boleta b
INNER JOIN tipo t on t.id=b.tipo
INNER JOIN banco ba on ba.id=b.banco
INNER JOIN estado e on e.id=b.estado
WHERE b.eliminado=0 
AND b.estado IN(' . $d . ') 
AND b.moneda=:moneda
Order By b.fecha_valido asc');
            $restresult->execute(array('moneda' => $moneda));
            $r = array();
            if ($restresult === null) {
                return new View("Sin resultados", Response::HTTP_NOT_FOUND);
            }
            $fecha = new \DateTime();
            foreach ($restresult->fetchAll() as $value) {
                $childrens = $this->getChildrens($value['renovado_por']);
                $childrens = array_reverse($childrens);
                $d = $fecha->diff(new \DateTime($value['fecha_valido']));
                $value['childrens'] = $childrens;
                $value['days'] = $d->days;
                $value['d'] = $d->d;
                $value['m'] = $d->m;
                $value['y'] = $d->y;
                $value['invert'] = $d->invert;
                array_push($r, $value);
            }
            if ($restresult === null) {
                return new View("Sin resultados", Response::HTTP_NOT_FOUND);
            }
            $view = $this->renderView('MPDBoletasBundle:Default:index.html.twig', array('data' => $r, 'moneda' => $moneda));
            $mpdf = new \Mpdf\Mpdf(['format' => 'Letter']);
            $mpdf->WriteHTML($view);
            $d = $mpdf->Output('', 'S');
            return new View(array('data' => base64_encode($d)), Response::HTTP_OK);
        } catch (Exception $e) {
            return new View($e->getMessage(), Response::HTTP_NOT_FOUND);
        }
        return new View("there are no users exist", Response::HTTP_NOT_FOUND);
    }

    private function getChildrens($value) {
        $em = $this->getDoctrine()->getManager();
        $restresult = $em->getConnection()->prepare('SELECT b.id,b.proveedor,b.monto,b.moneda,b.codigo,b.serie,b.fecha_valido,b.concepto,t.nombre as tipo,ba.nombre as banco,e.nombre as estado,b.observaciones,b.renovado_por
FROM boleta b
INNER JOIN tipo t on t.id=b.tipo
INNER JOIN banco ba on ba.id=b.banco
INNER JOIN estado e on e.id=b.estado
WHERE b.id=:id
AND b.eliminado=0 
Order By b.fecha_valido asc');
        $restresult->execute(array('id' => $value));
        $padre = $restresult->fetchAll();
        if (count($padre) > 0) {
            $r = $this->getChildrens($padre[0]['renovado_por']);
            //$padre->setRenovadoPor(null);
            array_push($r, $padre[0]);
            return $r;
        } else {
            return array();
        }
    }

    /**
     * @Route("/cumplimiento/")
     * @Method({"POST"})
     */
    public function postCumplimientoAction(Request $request) {
        $req = $request->headers->get('authorization');
        $jwt = $this->get('utilidades');
        $data = $jwt->decode($req);
        if ($data['status'] != 'ok') {

            return new View($data, Response::HTTP_NOT_FOUND);
        }
        $data = json_decode($request->getContent(), TRUE);
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction();
        try {
            if ($data['id'] != '') {
                $c = $this->getDoctrine()->getRepository('ModelBundle:Cumplimiento')->findOneBy(array('id' => $data['id']));
                $archivos = $em->getRepository('ModelBundle:ArchivoCumplimiento')->findBy(array('cumplimiento' => $c->getId()));
                foreach ($archivos as $archivo) {
                    $em->remove($archivo);
                }
            } else {
                $c = new Cumplimiento();
                $boleta = $this->getDoctrine()->getRepository('ModelBundle:Boleta')->findOneBy(array('id' => $data['boleta']));
                $estado = $this->getDoctrine()->getRepository('ModelBundle:Estado')->findOneBy(array('id' => '28'));
                $c->setBoleta($boleta);
                $boleta->setEstado($estado);
                $em->persist($boleta);
            }
            $fecha = new \DateTime($data['fecha']);
            $c->setFecha($fecha);
            $c->setHr($data['hr']);
            $c->setObs($data['observaciones']);
            $em->persist($c);
            foreach ($data['archivos'] as $archivo) {
                $a = new ArchivoCumplimiento();
                $a->setName($archivo['name']);
                $a->setSize($archivo['size']);
                $a->setUri($archivo['uri']);
                $a->setType($archivo['type']);
                $a->setIcon('icon');
                $a->setCumplimiento($c);
                $em->persist($a);
            }
            $em->flush();
            $em->getConnection()->commit();
            return new View(array('status' => Response::HTTP_OK), Response::HTTP_OK);
        } catch (Exception $e) {
            $em->getConnection()->rollBack();
            return new View(array('status' => Response::HTTP_NOT_FOUND, 'msg' => $e->getMessage(), 'line' => $e->getLine()), Response::HTTP_OK);
        }
    }

    /**
     * @Route("/ejecutado/")
     * @Method({"POST"})
     */
    public function postEjecutadoAction(Request $request) {
        $req = $request->headers->get('authorization');
        $jwt = $this->get('utilidades');
        $data = $jwt->decode($req);
        if ($data['status'] != 'ok') {

            return new View($data, Response::HTTP_NOT_FOUND);
        }
        $data = json_decode($request->getContent(), TRUE);
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction();
        try {
            if ($data['id'] != '') {
                $c = $this->getDoctrine()->getRepository('ModelBundle:Ejecutado')->findOneBy(array('id' => $data['id']));
                $archivos = $em->getRepository('ModelBundle:ArchivoEjecutado')->findBy(array('ejecutado' => $c->getId()));
                foreach ($archivos as $archivo) {
                    $em->remove($archivo);
                }
            } else {
                $c = new Ejecutado();
                $boleta = $this->getDoctrine()->getRepository('ModelBundle:Boleta')->findOneBy(array('id' => $data['boleta']));
                $estado = $this->getDoctrine()->getRepository('ModelBundle:Estado')->findOneBy(array('id' => '27'));
                $c->setBoleta($boleta);
                $boleta->setEstado($estado);
                $em->persist($boleta);
            }
            $fecha = new \DateTime($data['fecha']);
            $c->setFecha($fecha);
            $c->setHr($data['hr']);
            $c->setObs($data['observaciones']);
            $em->persist($c);
            foreach ($data['archivos'] as $archivo) {
                $a = new ArchivoEjecutado();
                $a->setName($archivo['name']);
                $a->setSize($archivo['size']);
                $a->setUri($archivo['uri']);
                $a->setType($archivo['type']);
                $a->setIcon('icon');
                $a->setEjecutado($c);
                $em->persist($a);
            }
            $em->flush();
            $em->getConnection()->commit();
            return new View(array('status' => Response::HTTP_OK), Response::HTTP_OK);
        } catch (Exception $e) {
            $em->getConnection()->rollBack();
            return new View(array('status' => Response::HTTP_NOT_FOUND, 'msg' => $e->getMessage(), 'line' => $e->getLine()), Response::HTTP_OK);
        }
    }

    private function getEnRojo() {
        $em = $this->getDoctrine()->getManager();
        $q = $em->getConnection()->prepare('SELECT * FROM (SELECT *,DATEDIFF(b.fecha_valido,curdate()) as dias
			FROM boleta b
			WHERE b.eliminado=0 AND b.estado IN(1,4,26))as t
WHERE t.dias<=15 AND t.dias>0');
        $q->execute();
        return $q->fetchAll();
    }

    private function getEnAmarillo() {
        $em = $this->getDoctrine()->getManager();
        $q = $em->getConnection()->prepare('SELECT * FROM (SELECT *,DATEDIFF(b.fecha_valido,curdate()) as dias
			FROM boleta b
			WHERE b.eliminado=0 AND b.estado IN(1,4,26))as t
WHERE t.dias<=20 AND t.dias>15');
        $q->execute();
        return $q->fetchAll();
    }

    private function getEnVerde() {
        $em = $this->getDoctrine()->getManager();
        $q = $em->getConnection()->prepare('SELECT * FROM (SELECT *,DATEDIFF(b.fecha_valido,curdate()) as dias
			FROM boleta b
			WHERE b.eliminado=0 AND b.estado IN(1,4,26))as t
WHERE t.dias>20');
        $q->execute();
        return $q->fetchAll();
    }

    private function getVencidos() {
        $em = $this->getDoctrine()->getManager();
        $q = $em->getConnection()->prepare('SELECT * FROM (SELECT *,DATEDIFF(b.fecha_valido,curdate()) as dias
			FROM boleta b
			WHERE b.eliminado=0 AND b.estado IN(1,4,26))as t
WHERE t.dias<0');
        $q->execute();
        return $q->fetchAll();
    }

    /**
     * @Route("/color/{val}")
     * @Method({"GET"})
     */
    public function porColorAction(Request $request, $val) {
        $req = $request->headers->get('authorization');
        $jwt = $this->get('utilidades');
        $data = $jwt->decode($req);
        if ($data['status'] != 'ok') {

            return new View($data, Response::HTTP_NOT_FOUND);
        }
        try {
            if ($val == 0) {
                $r = $this->getEnRojo();
            }
            if ($val == 1) {
                $r = $this->getEnAmarillo();
            }
            if ($val == 2) {
                $r = $this->getEnVerde();
            }
            if ($val == 3) {
                $r = $this->getVencidos();
            }

            if ($r === null) {
                return new View("Sin resultados", Response::HTTP_NOT_FOUND);
            }

            return new View(array('status' => Response::HTTP_OK, 'data' => $r), Response::HTTP_OK);
        } catch (Exception $e) {
            return new View($e->getMessage(), Response::HTTP_NOT_FOUND);
        }
        return new View("there are no users exist", Response::HTTP_NOT_FOUND);
    }

}
