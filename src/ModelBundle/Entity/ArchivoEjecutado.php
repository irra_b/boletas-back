<?php

namespace ModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ArchivoEjecutado
 *
 * @ORM\Table(name="archivo_ejecutado", indexes={@ORM\Index(name="fk1111", columns={"ejecutado"})})
 * @ORM\Entity
 */
class ArchivoEjecutado
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="size", type="float", precision=10, scale=0, nullable=false)
     */
    private $size;

    /**
     * @var string
     *
     * @ORM\Column(name="uri", type="string", length=255, nullable=false)
     */
    private $uri;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="icon", type="string", length=255, nullable=true)
     */
    private $icon;

    /**
     * @var \Ejecutado
     *
     * @ORM\ManyToOne(targetEntity="Ejecutado")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ejecutado", referencedColumnName="id")
     * })
     */
    private $ejecutado;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ArchivoEjecutado
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set size
     *
     * @param float $size
     *
     * @return ArchivoEjecutado
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return float
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set uri
     *
     * @param string $uri
     *
     * @return ArchivoEjecutado
     */
    public function setUri($uri)
    {
        $this->uri = $uri;

        return $this;
    }

    /**
     * Get uri
     *
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return ArchivoEjecutado
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set icon
     *
     * @param string $icon
     *
     * @return ArchivoEjecutado
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set ejecutado
     *
     * @param \ModelBundle\Entity\Ejecutado $ejecutado
     *
     * @return ArchivoEjecutado
     */
    public function setEjecutado(\ModelBundle\Entity\Ejecutado $ejecutado = null)
    {
        $this->ejecutado = $ejecutado;

        return $this;
    }

    /**
     * Get ejecutado
     *
     * @return \ModelBundle\Entity\Ejecutado
     */
    public function getEjecutado()
    {
        return $this->ejecutado;
    }
}
