<?php

namespace ModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cumplimiento
 *
 * @ORM\Table(name="cumplimiento", indexes={@ORM\Index(name="fk_c", columns={"boleta"})})
 * @ORM\Entity
 */
class Cumplimiento
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="hr", type="string", length=100, nullable=true)
     */
    private $hr;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=false)
     */
    private $fecha;

    /**
     * @var string
     *
     * @ORM\Column(name="obs", type="string", length=1000, nullable=true)
     */
    private $obs;

    /**
     * @var \Boleta
     *
     * @ORM\ManyToOne(targetEntity="Boleta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="boleta", referencedColumnName="id")
     * })
     */
    private $boleta;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set hr
     *
     * @param string $hr
     *
     * @return Cumplimiento
     */
    public function setHr($hr)
    {
        $this->hr = $hr;

        return $this;
    }

    /**
     * Get hr
     *
     * @return string
     */
    public function getHr()
    {
        return $this->hr;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return Cumplimiento
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set obs
     *
     * @param string $obs
     *
     * @return Cumplimiento
     */
    public function setObs($obs)
    {
        $this->obs = $obs;

        return $this;
    }

    /**
     * Get obs
     *
     * @return string
     */
    public function getObs()
    {
        return $this->obs;
    }

    /**
     * Set boleta
     *
     * @param \ModelBundle\Entity\Boleta $boleta
     *
     * @return Cumplimiento
     */
    public function setBoleta(\ModelBundle\Entity\Boleta $boleta = null)
    {
        $this->boleta = $boleta;

        return $this;
    }

    /**
     * Get boleta
     *
     * @return \ModelBundle\Entity\Boleta
     */
    public function getBoleta()
    {
        return $this->boleta;
    }
}
