<?php

namespace ModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ejecutado
 *
 * @ORM\Table(name="ejecutado", indexes={@ORM\Index(name="fk_e", columns={"boleta"})})
 * @ORM\Entity
 */
class Ejecutado
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="hr", type="string", length=100, nullable=true)
     */
    private $hr;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=false)
     */
    private $fecha;

    /**
     * @var string
     *
     * @ORM\Column(name="obs", type="string", length=1000, nullable=true)
     */
    private $obs;

    /**
     * @var string
     *
     * @ORM\Column(name="resolucion", type="string", length=100, nullable=false)
     */
    private $resolucion;

    /**
     * @var \Boleta
     *
     * @ORM\ManyToOne(targetEntity="Boleta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="boleta", referencedColumnName="id")
     * })
     */
    private $boleta;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set hr
     *
     * @param string $hr
     *
     * @return Ejecutado
     */
    public function setHr($hr)
    {
        $this->hr = $hr;

        return $this;
    }

    /**
     * Get hr
     *
     * @return string
     */
    public function getHr()
    {
        return $this->hr;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return Ejecutado
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set obs
     *
     * @param string $obs
     *
     * @return Ejecutado
     */
    public function setObs($obs)
    {
        $this->obs = $obs;

        return $this;
    }

    /**
     * Get obs
     *
     * @return string
     */
    public function getObs()
    {
        return $this->obs;
    }

    /**
     * Set resolucion
     *
     * @param string $resolucion
     *
     * @return Ejecutado
     */
    public function setResolucion($resolucion)
    {
        $this->resolucion = $resolucion;

        return $this;
    }

    /**
     * Get resolucion
     *
     * @return string
     */
    public function getResolucion()
    {
        return $this->resolucion;
    }

    /**
     * Set boleta
     *
     * @param \ModelBundle\Entity\Boleta $boleta
     *
     * @return Ejecutado
     */
    public function setBoleta(\ModelBundle\Entity\Boleta $boleta = null)
    {
        $this->boleta = $boleta;

        return $this;
    }

    /**
     * Get boleta
     *
     * @return \ModelBundle\Entity\Boleta
     */
    public function getBoleta()
    {
        return $this->boleta;
    }
}
