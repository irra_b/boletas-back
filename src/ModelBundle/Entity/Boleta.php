<?php

namespace ModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Boleta
 *
 * @ORM\Table(name="boleta", indexes={@ORM\Index(name="fk1", columns={"tipo"}), @ORM\Index(name="fk2", columns={"banco"}), @ORM\Index(name="fk3", columns={"beneficiario"}), @ORM\Index(name="fk4", columns={"estado"}), @ORM\Index(name="fk5", columns={"renovado_por"})})
 * @ORM\Entity
 */
class Boleta
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="proveedor", type="string", length=500, nullable=false)
     */
    private $proveedor;

    /**
     * @var float
     *
     * @ORM\Column(name="monto", type="float", precision=10, scale=0, nullable=false)
     */
    private $monto;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=255, nullable=false)
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="serie", type="string", length=255, nullable=true)
     */
    private $serie;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_valido", type="date", nullable=false)
     */
    private $fechaValido;

    /**
     * @var string
     *
     * @ORM\Column(name="concepto", type="string", length=2000, nullable=false)
     */
    private $concepto;

    /**
     * @var integer
     *
     * @ORM\Column(name="eliminado", type="integer", nullable=false)
     */
    private $eliminado;

    /**
     * @var string
     *
     * @ORM\Column(name="observaciones", type="string", length=1000, nullable=true)
     */
    private $observaciones;

    /**
     * @var boolean
     *
     * @ORM\Column(name="editable", type="boolean", nullable=true)
     */
    private $editable;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_modificado", type="datetime", nullable=true)
     */
    private $fechaModificado;

    /**
     * @var string
     *
     * @ORM\Column(name="moneda", type="string", length=255, nullable=false)
     */
    private $moneda;

    /**
     * @var string
     *
     * @ORM\Column(name="uri", type="text", length=65535, nullable=true)
     */
    private $uri;

    /**
     * @var \Tipo
     *
     * @ORM\ManyToOne(targetEntity="Tipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipo", referencedColumnName="id")
     * })
     */
    private $tipo;

    /**
     * @var \Banco
     *
     * @ORM\ManyToOne(targetEntity="Banco")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="banco", referencedColumnName="id")
     * })
     */
    private $banco;

    /**
     * @var \Beneficiario
     *
     * @ORM\ManyToOne(targetEntity="Beneficiario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="beneficiario", referencedColumnName="id")
     * })
     */
    private $beneficiario;

    /**
     * @var \Estado
     *
     * @ORM\ManyToOne(targetEntity="Estado")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="estado", referencedColumnName="id")
     * })
     */
    private $estado;

    /**
     * @var \Boleta
     *
     * @ORM\ManyToOne(targetEntity="Boleta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="renovado_por", referencedColumnName="id")
     * })
     */
    private $renovadoPor;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set proveedor
     *
     * @param string $proveedor
     *
     * @return Boleta
     */
    public function setProveedor($proveedor)
    {
        $this->proveedor = $proveedor;

        return $this;
    }

    /**
     * Get proveedor
     *
     * @return string
     */
    public function getProveedor()
    {
        return $this->proveedor;
    }

    /**
     * Set monto
     *
     * @param float $monto
     *
     * @return Boleta
     */
    public function setMonto($monto)
    {
        $this->monto = $monto;

        return $this;
    }

    /**
     * Get monto
     *
     * @return float
     */
    public function getMonto()
    {
        return $this->monto;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     *
     * @return Boleta
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set serie
     *
     * @param string $serie
     *
     * @return Boleta
     */
    public function setSerie($serie)
    {
        $this->serie = $serie;

        return $this;
    }

    /**
     * Get serie
     *
     * @return string
     */
    public function getSerie()
    {
        return $this->serie;
    }

    /**
     * Set fechaValido
     *
     * @param \DateTime $fechaValido
     *
     * @return Boleta
     */
    public function setFechaValido($fechaValido)
    {
        $this->fechaValido = $fechaValido;

        return $this;
    }

    /**
     * Get fechaValido
     *
     * @return \DateTime
     */
    public function getFechaValido()
    {
        return $this->fechaValido;
    }

    /**
     * Set concepto
     *
     * @param string $concepto
     *
     * @return Boleta
     */
    public function setConcepto($concepto)
    {
        $this->concepto = $concepto;

        return $this;
    }

    /**
     * Get concepto
     *
     * @return string
     */
    public function getConcepto()
    {
        return $this->concepto;
    }

    /**
     * Set eliminado
     *
     * @param integer $eliminado
     *
     * @return Boleta
     */
    public function setEliminado($eliminado)
    {
        $this->eliminado = $eliminado;

        return $this;
    }

    /**
     * Get eliminado
     *
     * @return integer
     */
    public function getEliminado()
    {
        return $this->eliminado;
    }

    /**
     * Set observaciones
     *
     * @param string $observaciones
     *
     * @return Boleta
     */
    public function setObservaciones($observaciones)
    {
        $this->observaciones = $observaciones;

        return $this;
    }

    /**
     * Get observaciones
     *
     * @return string
     */
    public function getObservaciones()
    {
        return $this->observaciones;
    }

    /**
     * Set editable
     *
     * @param boolean $editable
     *
     * @return Boleta
     */
    public function setEditable($editable)
    {
        $this->editable = $editable;

        return $this;
    }

    /**
     * Get editable
     *
     * @return boolean
     */
    public function getEditable()
    {
        return $this->editable;
    }

    /**
     * Set fechaModificado
     *
     * @param \DateTime $fechaModificado
     *
     * @return Boleta
     */
    public function setFechaModificado($fechaModificado)
    {
        $this->fechaModificado = $fechaModificado;

        return $this;
    }

    /**
     * Get fechaModificado
     *
     * @return \DateTime
     */
    public function getFechaModificado()
    {
        return $this->fechaModificado;
    }

    /**
     * Set moneda
     *
     * @param string $moneda
     *
     * @return Boleta
     */
    public function setMoneda($moneda)
    {
        $this->moneda = $moneda;

        return $this;
    }

    /**
     * Get moneda
     *
     * @return string
     */
    public function getMoneda()
    {
        return $this->moneda;
    }

    /**
     * Set uri
     *
     * @param string $uri
     *
     * @return Boleta
     */
    public function setUri($uri)
    {
        $this->uri = $uri;

        return $this;
    }

    /**
     * Get uri
     *
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * Set tipo
     *
     * @param \ModelBundle\Entity\Tipo $tipo
     *
     * @return Boleta
     */
    public function setTipo(\ModelBundle\Entity\Tipo $tipo = null)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return \ModelBundle\Entity\Tipo
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set banco
     *
     * @param \ModelBundle\Entity\Banco $banco
     *
     * @return Boleta
     */
    public function setBanco(\ModelBundle\Entity\Banco $banco = null)
    {
        $this->banco = $banco;

        return $this;
    }

    /**
     * Get banco
     *
     * @return \ModelBundle\Entity\Banco
     */
    public function getBanco()
    {
        return $this->banco;
    }

    /**
     * Set beneficiario
     *
     * @param \ModelBundle\Entity\Beneficiario $beneficiario
     *
     * @return Boleta
     */
    public function setBeneficiario(\ModelBundle\Entity\Beneficiario $beneficiario = null)
    {
        $this->beneficiario = $beneficiario;

        return $this;
    }

    /**
     * Get beneficiario
     *
     * @return \ModelBundle\Entity\Beneficiario
     */
    public function getBeneficiario()
    {
        return $this->beneficiario;
    }

    /**
     * Set estado
     *
     * @param \ModelBundle\Entity\Estado $estado
     *
     * @return Boleta
     */
    public function setEstado(\ModelBundle\Entity\Estado $estado = null)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return \ModelBundle\Entity\Estado
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set renovadoPor
     *
     * @param \ModelBundle\Entity\Boleta $renovadoPor
     *
     * @return Boleta
     */
    public function setRenovadoPor(\ModelBundle\Entity\Boleta $renovadoPor = null)
    {
        $this->renovadoPor = $renovadoPor;

        return $this;
    }

    /**
     * Get renovadoPor
     *
     * @return \ModelBundle\Entity\Boleta
     */
    public function getRenovadoPor()
    {
        return $this->renovadoPor;
    }
}
