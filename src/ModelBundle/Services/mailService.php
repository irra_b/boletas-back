<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace ModelBundle\Services;
use ModelBundle\Entity\Config;

/**
 * Description of jwtAuth
 *
 * @author irra
 */
use Firebase\JWT\JWT;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;

class mailService {

    //put your code here
    private $templating;
    public function __construct($templating) {
        $this->templating = $templating;
    }

    public function send($to, $subject, $view,$data,$config) {
        //['israel.rollano@planificacion.gob.bo'=>'Boletas de garantía', 'irra.br@gmail.com' => 'Boletas de garantía']
        $host=$this->getValue($config, 'host');
        $port=$this->getValue($config, 'port');
        $encrypt=$this->getValue($config, 'encrypt');
        $user=$this->getValue($config, 'userName');
        $password=$this->getValue($config, 'password');
        $alias=$this->getValue($config, 'alias');
        try {
            $transport = (new \Swift_SmtpTransport($host['value'], $port['value'], $encrypt['value']))
                    ->setUsername($user['value'])
                    ->setPassword($password['value'])
                    ->setStreamOptions(array('ssl' => array('verify_peer' => FALSE, 'verify_peer_name' => FALSE, 'allow_self_signed' => TRUE)))
            ;
            $mailer = new \Swift_Mailer($transport);
            $message = (new \Swift_Message($subject))
                    ->setFrom([$user['value'] => $alias['value']])
                    ->setTo($to)
                    ->setBody($this->templating->Render($view,array('data'=>$data)),'text/html');
            $r=$mailer->send($message);
            return $r;
        } catch (Exception $e) {
            return $e;
        }
    }
    private function getValue($config,$key){
        $index=array_search($key, array_column($config, 'key'));
        return $config[$index];
    }

}
