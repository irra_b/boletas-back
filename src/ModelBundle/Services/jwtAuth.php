<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace ModelBundle\Services;

/**
 * Description of jwtAuth
 *
 * @author irra
 */
use Firebase\JWT\JWT;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;

class jwtAuth {

    //put your code here
    private $manager;
    private $key = 'this-is-a-secret-key';

    public function __construct($manager) {
        $this->manager = $manager;
    }

    public function decode($token) {
        try {
            $d = JWT::decode($token, $this->key, array('HS256'));
            return array('status' => "ok",'user'=>$d->sub,'rol'=>$d->role);
        } catch (\Firebase\JWT\ExpiredException $e) {
            return array('status' => "Expired Token");
        }
    }

    public function signup($user, $password, $hash = null) {

        $enc = new MessageDigestPasswordEncoder();
        $pass = $enc->encodePassword($password, 'my_salt');      
        $user = $this->manager->getRepository('ModelBundle:User')->findOneBy(array('email' => $user, 'password' => $pass));
        if (is_object($user)) {
            $token = array(
                'sub' => $user->getId(),
                'iat' => time(),
                'exp' => time() + (60 * 60 * 24 * 7),
            );
            $jwt = JWT::encode($token, $this->key, 'HS256');
            return array('status' => 'success', 'data' => 'login success', 'token' => $jwt, 'email' => $user->getEmail(), 'fullName' => $user->getNombre(), 'role' => $user->getRol(),'id'=>$user->getId(),'img'=>$user->getImg());
        }
        return array('status' => 'error', 'data' => 'login error');
    }
    public function forgotPassword($email,$hash = null) {

        $enc = new MessageDigestPasswordEncoder();
        $user = $this->manager->getRepository('ModelBundle:User')->findOneBy(array('email' => $email));
        if (is_object($user)) {
            $token = array(
                'sub' => $user->getEmail(),
                'iat' => time(),
                'exp' => time() + (60 * 15),
            );
            $jwt = JWT::encode($token, $this->key, 'HS256');
            return array('status' => 'success', 'token' => $jwt);
        }
        return array('status' => 'error', 'data' => 'login error');
    }
    public function verifyForgotPasswordToken($token) {
        try {
            $d = JWT::decode($token, $this->key, array('HS256'));
            return array('status' => "ok",'email'=>$d->sub);
        } catch (\Firebase\JWT\ExpiredException $e) {
            return array('status' => "Expired Token");
        }
    }

}
