<?php

namespace ModelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use \FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use ModelBundle\Entity\Rubro;

class DefaultController extends FOSRestController
{
    public function indexAction()
    {
        return $this->render('ModelBundle:Default:index.html.twig');
    }
    
}
