<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Firebase\JWT\JWT;
use Exception;
use ModelBundle\Entity\User;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;

class DefaultController extends FOSRestController {

    /**
     * @Route("/")
     * @Method({"POST"})
     */
    public function postAction(Request $req) {
        $d = json_decode($req->getContent(), true);
        $jwt = $this->get('utilidades');
        $data = $jwt->signup($d['email'], $d['password'], '');
        //$d=JWT::decode($jwt,$key,array('HS256'));
        //$restresult = $this->getDoctrine()->getRepository('ModelBundle:Rubro')->findOneBy(array('id'=>$id));
        if ($data['status'] == 'error') {
            return new View(array('status' => Response::HTTP_UNAUTHORIZED, 'data' => $data));
        } else {
            return new View(array('status' => Response::HTTP_OK, 'data' => $data));
        }
    }

    /**
     * @Route("/")
     * @Method({"GET"})
     */
    public function verifyAction(Request $request) {
        try {
            $req = $request->headers->get('authorization');
            $jwt = $this->get('utilidades');
            $data = $jwt->decode($req);
            if ($data['status'] == 'ok') {
                return new View(array('status' => Response::HTTP_OK));
            } else {
                return new View(array('status' => Response::HTTP_UNAUTHORIZED));
            }
        } catch (Exception $ex) {
            return new View(array('status' => Response::HTTP_CONFLICT, 'line' => $ex->getLine(), 'message' => $ex->getMessage()));
        }
    }

    /**
     * @Route("/verify/{pass}/{id}")
     * @Method({"GET"})
     */
    public function verifyPasswordAction(Request $request, $pass, $id) {
        try {
            $req = $request->headers->get('authorization');
            $jwt = $this->get('utilidades');
            $data = $jwt->decode($req);
            if ($data['status'] == 'ok') {
                $em = $this->getDoctrine()->getManager();
                $user = $this->getDoctrine()->getRepository('ModelBundle:User')->findOneBy(array('id' => $id));
                if ($user != NULL) {
                    $enc = new MessageDigestPasswordEncoder();
                    $pass = $enc->encodePassword($pass, 'my_salt');
                    if ($pass == $user->getPassword()) {
                        return new View(array('status' => Response::HTTP_OK), Response::HTTP_OK);
                    } else {
                        return new View(array('status' => Response::HTTP_UNAUTHORIZED), Response::HTTP_OK);
                    }
                } else {
                    return new View(array('status' => Response::HTTP_UNAUTHORIZED), Response::HTTP_OK);
                }
            } else {
                return new View(array('status' => Response::HTTP_UNAUTHORIZED), Response::HTTP_OK);
            }
        } catch (Exception $ex) {
            return new View(array('status' => Response::HTTP_UNAUTHORIZED), Response::HTTP_OK);
        }
    }

    /**
     * @Route("/pass/{id}")
     * @Method({"PUT"})
     */
    public function passwordAction(Request $request, $id) {
        $req = $request->headers->get('authorization');
        $jwt = $this->get('utilidades');
        $data = $jwt->decode($req);
        if ($data['status'] != 'ok') {
            return new View($data, Response::HTTP_NOT_FOUND);
        }
        $data = json_decode($request->getContent(), TRUE);
        if ($data['nueva'] == "") {
            return new View(array('status' => 401, 'msg' => 'No password'), Response::HTTP_OK);
        }
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction();
        try {

            $user = $this->getDoctrine()->getRepository('ModelBundle:User')->findOneBy(array('id' => $id));
            $enc = new MessageDigestPasswordEncoder();
            $pass = $enc->encodePassword($data['nueva'], 'my_salt');
            $user->setPassword($pass);
            $em->persist($user);
            $em->flush();
            $em->getConnection()->commit();
            return new View(array('status' => 200), Response::HTTP_OK);
        } catch (Exception $e) {
            $em->getConnection()->rollBack();
            return new View(array('status' => 401, 'msg' => $e->getMessage()), Response::HTTP_OK);
        }
    }

    /**
     * @Route("/forgot-password/")
     * @Method({"POST"})
     */
    public function forgotPasswordAction(Request $req) {
        $data = json_decode($req->getContent(), true);
        if (isset($data['email'])) {
            $user = $this->getDoctrine()->getRepository('ModelBundle:User')->findOneBy(array('email' => $data['email']));
            if ($user != NULL) {
                $jwt = $this->get('utilidades');
                $em = $this->getDoctrine()->getManager();
                $d = $jwt->forgotPassword($data['email'], '');
                if ($d['status'] == 'success') {
                    $config = $em->getConnection()->prepare('SELECT * FROM config');
                    $config->execute();
                    $config = $config->fetchAll();
                    $m = $this->get('mailer');
                    $r = $m->send([$data['email'] => 'Boletas de garantía'], 'Recuperar contraseña', 'forgot.html.twig', $d, $config);
                    return new View(array('status' => Response::HTTP_OK, 'data' => $data));
                } else {
                    return new View(array('status' => Response::HTTP_UNAUTHORIZED, 'msg' => 'Error al generar token'));
                }
            } else {
                return new View(array('status' => Response::HTTP_UNAUTHORIZED, 'msg' => 'El email no existe'));
            }
        } else {
            return new View(array('status' => Response::HTTP_UNAUTHORIZED, 'msg' => 'No email data'));
        }
    }

    /**
     * @Route("/recover-password/")
     * @Method({"POST"})
     */
    public function recoverPasswordAction(Request $req) {
        $data = json_decode($req->getContent(), true);
        $jwt = $this->get('utilidades');
        if (isset($data['password']) && isset($data['password2']) && isset($data['token'])) {
            $password=$data['password'];
            $data = $jwt->verifyForgotPasswordToken($data['token']);
            if ($data['status'] != 'ok') {
                return new View($data, Response::HTTP_NOT_FOUND);
            }
            $user = $this->getDoctrine()->getRepository('ModelBundle:User')->findOneBy(array('email' => $data['email']));
            if ($user != NULL) {
                $em = $this->getDoctrine()->getManager();
                $em->getConnection()->beginTransaction();
                try {
                    $enc = new MessageDigestPasswordEncoder();
                    $pass = $enc->encodePassword($password, 'my_salt');
                    $user->setPassword($pass);
                    $em->persist($user);
                    $em->flush();
                    $em->getConnection()->commit();
                    return new View(array('status' => 200), Response::HTTP_OK);
                } catch (Exception $e) {
                    $em->getConnection()->rollBack();
                    return new View(array('status' => 401, 'msg' => $e->getMessage()), Response::HTTP_OK);
                }
            } else {
                return new View(array('status' => Response::HTTP_UNAUTHORIZED, 'msg' => 'El email no existe'));
            }
        } else {
            return new View(array('status' => Response::HTTP_UNAUTHORIZED, 'msg' => 'No email data'));
        }
    }

}
