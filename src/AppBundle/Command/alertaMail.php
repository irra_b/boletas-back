<?php

namespace AppBundle\Command;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Output\Output;
use Symfony\Component\Console\Command\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

/**
 * Description of alertaMail
 *
 * @author irrab
 */
class alertaMail extends ContainerAwareCommand {

    protected function configure() {
        $this
                ->setName('email:por-vencer')
                ->setDefinition(array())
                ->setDescription('Envía un email con las boletas por vencer.')
                ->setHelp(<<<EOT
El comando <info>email:por-vencer</info> Envía un email con las boletas por vencer.
EOT
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $contenedor = $this->getContainer();
        $em = $contenedor->get('doctrine')->getManager();
        $restresult = $em->getConnection()->prepare('SELECT * 
FROM (
SELECT *,DATEDIFF(b.fecha_valido,curdate()) as dias
FROM boleta b
WHERE b.eliminado=0 AND b.estado IN(1,4,26))as t
WHERE t.dias<=:dias AND t.dias>0');
        $restresult->execute(array('dias' => 16));
        $r = $restresult->fetchAll();
        $config = $em->getConnection()->prepare('SELECT * FROM config');
        $config->execute();
        $config=$config->fetchAll();
        if (count($r) > 0) {
            $output->writeln(count($r) . ' Boletas por vencer...');
            $output->writeln('Enviando correo de alerta...');
            foreach ($this->getUsers() as $user) {
                $output->writeln('Enviando a '.$user['email']);
                //$user['email'] => 'Boletas de garantía',
                $this->send([$user['email']=>'Boletas de garantía','israel.rollano@planificacion.gob.bo'=>'Boletas'],$r,$config);
                $output->writeln('Enviado');
            }
        } else {
            $output->writeln('No existen boletas por vencer...');
        }
    }

    private function send($to,$data,$config) {
        $contenedor = $this->getContainer();
        $m = $contenedor->get('mailer');
        $r = $m->send($to, 'Alerta Boletas de Garantía', 'base.html.twig', $data,$config);
    }

    private function getUsers() {
        $contenedor = $this->getContainer();
        $em = $contenedor->get('doctrine')->getManager();
        $restresult = $em->getConnection()->prepare('SELECT * 
FROM user');
        $restresult->execute();
        $r = $restresult->fetchAll();
        return $r;
    }

}
