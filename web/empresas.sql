/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50719
Source Host           : localhost:3306
Source Database       : empresas

Target Server Type    : MYSQL
Target Server Version : 50719
File Encoding         : 65001

Date: 2017-10-08 20:57:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `contacto`
-- ----------------------------
DROP TABLE IF EXISTS `contacto`;
CREATE TABLE `contacto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  `empresa` int(11) NOT NULL,
  `medio_contacto` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk1` (`empresa`),
  CONSTRAINT `fk1` FOREIGN KEY (`empresa`) REFERENCES `empresa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of contacto
-- ----------------------------
INSERT INTO `contacto` VALUES ('30', 'sdfsdfsdf sdf sdf sdf', 'irra_b@yahoo.es', '4234234', '30', 'werwer');
INSERT INTO `contacto` VALUES ('31', 'xxxxxxxxxxxxxxxxx', 'irra_b@yahoo.es', '22222222222', '31', 'xxxxxxxxxxxx');
INSERT INTO `contacto` VALUES ('32', 'sdfsdfsdf sdf sdf sdf', 'irra_b@yahoo.es', '4234234', '30', 'werwer');
INSERT INTO `contacto` VALUES ('33', 'sdfsdfsdf sdf sdf sdf', 'irra_b@yahoo.es', '4234234', '30', 'werwer');
INSERT INTO `contacto` VALUES ('34', 'sdfsdfsdf sdf sdf sdf', 'irra_b@yahoo.es', '4234234', '30', 'werwer');
INSERT INTO `contacto` VALUES ('35', 'hernan siles', 'hernan@hurq.com', '2811225', '32', 'email');
INSERT INTO `contacto` VALUES ('36', 'zxczxc', 'JUAN@CBN.COM', '3232323', '33', 'werwer');

-- ----------------------------
-- Table structure for `departamento`
-- ----------------------------
DROP TABLE IF EXISTS `departamento`;
CREATE TABLE `departamento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of departamento
-- ----------------------------
INSERT INTO `departamento` VALUES ('1', 'Beni');
INSERT INTO `departamento` VALUES ('2', 'Chuquisaca');
INSERT INTO `departamento` VALUES ('3', 'Cochabamba');
INSERT INTO `departamento` VALUES ('4', 'La Paz');
INSERT INTO `departamento` VALUES ('5', 'Oruro');
INSERT INTO `departamento` VALUES ('6', 'Pando');
INSERT INTO `departamento` VALUES ('7', 'Potosí');
INSERT INTO `departamento` VALUES ('8', 'Santa Cruz');
INSERT INTO `departamento` VALUES ('9', 'Tarija');

-- ----------------------------
-- Table structure for `emp_enl`
-- ----------------------------
DROP TABLE IF EXISTS `emp_enl`;
CREATE TABLE `emp_enl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` varchar(50) NOT NULL,
  `respuesta` varchar(255) NOT NULL,
  `obs` varchar(255) NOT NULL,
  `empresa` int(11) NOT NULL,
  `enlace` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_empresa` (`empresa`),
  KEY `fk_enlace` (`enlace`),
  CONSTRAINT `fk_empresa` FOREIGN KEY (`empresa`) REFERENCES `empresa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_enlace` FOREIGN KEY (`enlace`) REFERENCES `enlace` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of emp_enl
-- ----------------------------

-- ----------------------------
-- Table structure for `empresa`
-- ----------------------------
DROP TABLE IF EXISTS `empresa`;
CREATE TABLE `empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `nit` varchar(255) NOT NULL,
  `tipo` int(11) NOT NULL,
  `rubro` int(11) NOT NULL,
  `departamento` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  `icon` varchar(100) DEFAULT NULL,
  `label` varchar(100) DEFAULT NULL,
  `enlace` int(11) DEFAULT NULL,
  `direccion` varchar(500) DEFAULT NULL,
  `observaciones` varchar(2000) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_1` (`tipo`),
  KEY `fk_2` (`rubro`),
  KEY `fk_3` (`departamento`),
  KEY `fk_4` (`enlace`),
  KEY `fk_5` (`estado`),
  CONSTRAINT `fk_1` FOREIGN KEY (`tipo`) REFERENCES `tipo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_2` FOREIGN KEY (`rubro`) REFERENCES `rubro` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_3` FOREIGN KEY (`departamento`) REFERENCES `departamento` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_4` FOREIGN KEY (`enlace`) REFERENCES `enlace` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_5` FOREIGN KEY (`estado`) REFERENCES `estado` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of empresa
-- ----------------------------
INSERT INTO `empresa` VALUES ('30', 'LAFAR', '4343434344', '1', '1499', '4', '2', '-18.5841', '-67.0122', './assets/markers/been-here-marker-green.png', null, '1', 'sdfsdfsdf', 'asdf', '0');
INSERT INTO `empresa` VALUES ('31', 'xxxxxxxxxx', '11111111111111', '1', '1499', '1', '1', '-16.4917', '-68.3456', './assets/markers/been-here-marker-green.png', null, '1', 'xxxxxxxxxxxxxxxxxxxx', 'asd', '0');
INSERT INTO `empresa` VALUES ('32', 'CERBECERIA BOLIVIANA NACIONAL', '2514521525', '1', '1509', '4', '2', '-16.489', '-68.1437', './assets/markers/been-here-marker-green.png', null, '1', 'SU DIRECCION', 'asd', '0');
INSERT INTO `empresa` VALUES ('33', 'xxx', '112345456666', '1', '1500', '1', '1', '-16.1309', '-67.4162', './assets/markers/been-here-marker-green.png', null, '1', 'zxczxc', 'asd', '0');

-- ----------------------------
-- Table structure for `enlace`
-- ----------------------------
DROP TABLE IF EXISTS `enlace`;
CREATE TABLE `enlace` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `ci` varchar(255) NOT NULL,
  `cargo` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of enlace
-- ----------------------------
INSERT INTO `enlace` VALUES ('1', 'juan', '60606060', 'xxx');

-- ----------------------------
-- Table structure for `estado`
-- ----------------------------
DROP TABLE IF EXISTS `estado`;
CREATE TABLE `estado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `icon` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of estado
-- ----------------------------
INSERT INTO `estado` VALUES ('1', 'Registrada', './assets/markers/been-here-marker-green.png');
INSERT INTO `estado` VALUES ('2', 'En Proceso', './assets/markers/been-here-marker-purple.png');
INSERT INTO `estado` VALUES ('3', 'Asociado', './assets/markers/been-here-marker-red.png');

-- ----------------------------
-- Table structure for `rubro`
-- ----------------------------
DROP TABLE IF EXISTS `rubro`;
CREATE TABLE `rubro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1517 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rubro
-- ----------------------------
INSERT INTO `rubro` VALUES ('1498', 'Actividad petrolera');
INSERT INTO `rubro` VALUES ('1499', 'Elaboración de productos alimenticios y bebidas');
INSERT INTO `rubro` VALUES ('1500', 'Elaboración de bebidas');
INSERT INTO `rubro` VALUES ('1501', 'Elaboración de productos de tabaco');
INSERT INTO `rubro` VALUES ('1502', 'Fabricación de productos textiles');
INSERT INTO `rubro` VALUES ('1503', 'Fabricación de prendas de vestir, adoboy teñido de pieles');
INSERT INTO `rubro` VALUES ('1504', 'Curtido y adobo de cueros; fabricación de maletas, bolsos de mano, artículos de talartería y cuarnicionería, y calzados');
INSERT INTO `rubro` VALUES ('1505', 'Productos de madera y fabricación de productos de madera y corcho, excepto muebles; fabricación de artículos de paja y de materiales trenzables');
INSERT INTO `rubro` VALUES ('1506', 'Fabricación de papel y productos de papel');
INSERT INTO `rubro` VALUES ('1507', 'Actividades de edición e impresión y de producción de grabaciones');
INSERT INTO `rubro` VALUES ('1508', 'Fabricación de productos de la refinación del petróleo');
INSERT INTO `rubro` VALUES ('1509', 'Fabricación de sustancias y productos quimicos');
INSERT INTO `rubro` VALUES ('1510', 'Fabricación de productos de caucho y plástico');
INSERT INTO `rubro` VALUES ('1511', 'Fabricación de otros productos minerales no metálicos');
INSERT INTO `rubro` VALUES ('1512', 'Fabricación de metales comunes');
INSERT INTO `rubro` VALUES ('1513', 'Fabricación de productos elaborados de metal, excepto maquinaria y equipo');
INSERT INTO `rubro` VALUES ('1514', 'Fabricación de maquinaria y equipo');
INSERT INTO `rubro` VALUES ('1515', 'Fabricación de equipo y aparatos de radio, televisión y comunicaciones');
INSERT INTO `rubro` VALUES ('1516', 'Fabricación de instrumentos médicos, ópticos y de precisión');

-- ----------------------------
-- Table structure for `tipo`
-- ----------------------------
DROP TABLE IF EXISTS `tipo`;
CREATE TABLE `tipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tipo
-- ----------------------------
INSERT INTO `tipo` VALUES ('1', 'Sociedad Colectiva (S. C.)');
INSERT INTO `tipo` VALUES ('22', 'Sociedad en Comandita Simple (S. C. S.)');
INSERT INTO `tipo` VALUES ('23', 'Sociedad de Responsabilidad Limitada (S .R. L.)');
INSERT INTO `tipo` VALUES ('24', 'Sociedad Anónima (S. A.)');
INSERT INTO `tipo` VALUES ('25', 'Sociedad en Comandita por Acciones (S. C. A.)');
INSERT INTO `tipo` VALUES ('26', 'Asociación Accidental o de cuentas en participació');
INSERT INTO `tipo` VALUES ('27', 'Las sociedades cooperativas');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `cargo` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('irra_b@yahoo.es', '123456', '1', '2017-09-30 22:43:42', '2017-09-30 22:43:42', 'Israel rollano', 'desarrolador');
